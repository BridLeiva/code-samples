### Solid Apps Landing
The landing page for Solid Apps GT. Where you can contact me or take a look at our various projects.
http://www.solidapps.com.gt


### Coding patterns
Most of the projects I have worked on include an NDA with my client or employer so I can't share the code directly. That's why I have my "notes on coding patterns", which are my way of keeping notes for myself and examples of my code for other people, these are design patterns I have encountered 'in the wild' so they are not all there.

The prototype pattern is one of my favorites, make sure you take a look at it:

https://nbviewer.jupyter.org/urls/gitlab.com/BridLeiva/coding-patterns/raw/master/coding_patterns.ipynb


### Plotly demo
A demo of data analysis using plotly
https://nbviewer.jupyter.org/urls/gitlab.com/BridLeiva/plotly-demo/raw/master/plotly%20demo.ipynb


### Oprotime
One live example of a system for a local client can be found below. It is in spanish and I cannot share the code but it is a good example of the kind of apps I deploy to heroku during development for the client to start using the functionality and give feedback as true agile development demands.

http://afternoon-eyrie-78302.herokuapp.com/


### MedTLDR
MedTLD is a software that allows searching for and summarizing medical or scientific papers using several different algorithms.

The UI is simple as requested for the client. This is a demo so anyone can register and use it for free.

MedTLDR-Search
http://tldr-demo.herokuapp.com/search

MedTLDR-Summaryzer 
http://tldr-demo.herokuapp.com/home


### Physics of time
An example in python of very rudimentary UI and game development, which is a simulation for studying entropy, can be seen at: 

https://gitlab.com/BridLeiva/physics-of-time

it's fully functional and can be installed in any system that supports python and pygame.
